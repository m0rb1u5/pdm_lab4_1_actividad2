package com.m0rb1u5.pdm_lab4_1_actividad2;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends ActionBarActivity {

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
   }

   @Override
   protected void onPause() {
      super.onPause();
      Log.d("Hello World", "onPause");
   }

   @Override
   protected void onResume() {
      super.onResume();
      Log.d("Hello World", "onResume");
   }

   @Override
   protected void onDestroy() {
      super.onDestroy();
      Log.d("Hello World", "onDestroy");
   }

   @Override
   protected void onRestart() {
      super.onRestart();
      Log.d("Hello World", "onRestart");
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_main, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      //noinspection SimplifiableIfStatement
      if (id == R.id.action_settings) {
         return true;
      }

      return super.onOptionsItemSelected(item);
   }
}
